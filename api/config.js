let apiBase = process.env.HOST_URL_API
let staticApiBase = process.env.HOST_URL_STATIC
let appApiBase = process.env.HOST_URL_APP

export default {
  apiBase,
  staticApiBase,
  appApiBase
}
