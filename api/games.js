import config from './config'
import axios from 'axios'

const getGameByGID = (gid) => {
  return axios.get(`${config.apiBase}/games/${gid}`)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
}

const getGamesByUIDs = (uids) => {
  let promises = []

  for (let i = 0; i < uids.length; i++) {
    promises.push(
      axios.get(`${config.apiBase}/games?field=uid&q=${uids[i]}`)
      .then(response => {
        let data = response.data

        if (!data.success) {
          return Promise.reject(data.message)
        }

        return data.games
      })
    )
  }

  return Promise.all(promises)
  .then(arrays => {
    let games = [].concat.apply([], arrays)
    games = games.filter(g => g)
    return games
  })
}

const getGames = (browser, device) => {
  return axios.get(`${config.apiBase}/games?browser=${browser}&device=${device}`)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.games
  })
}

const createGame = ({ title, description, price }) => {
  return axios.post(`${config.apiBase}/games`, {
    title,
    description,
    price
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateGameTitle = (gid, title) => {
  return axios.post(`${config.apiBase}/games/${gid}/title`, {
    title
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const udpateGameDescription = (gid, description) => {
  return axios.post(`${config.apiBase}/games/${gid}/description`, {
    description
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateGamePrice = (gid, price) => {
  console.log('Price: ', price)
  return axios.post(`${config.apiBase}/games/${gid}/price`, {
    price
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateGamePrivacy = (gid, privacy) => {
  return axios.post(`${config.apiBase}/games/${gid}/privacy`, {
    privacy
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateGameBrowserSupport = (gid, support) => {
  return axios.post(`${config.apiBase}/games/${gid}/system/browser`, {
    support
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateGameThumbnail = (gid, thumbnail) => {
  return axios.post(`${config.staticApiBase}/games/${gid}/thumbnail`, thumbnail)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateGameImages = (gid, images) => {
  return axios.post(`${config.staticApiBase}/games/${gid}/images`, images)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateGameVideos = (gid, videos) => {
  return axios.post(`${config.staticApiBase}/games/${gid}/videos`, {
    videos
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.game
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const createGameApp = (appForm) => {
  return axios.post(`${config.appApiBase}/apps`, appForm)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.app
  })
  .catch(err => {
    return Promise.reject(err.response.data.message || err)
  })
}

const updateGameAppFile = (aid, appForm) => {
  return axios.post(`${config.appApiBase}/apps/${aid}/file`, appForm)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.app
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateGameAppVersion = (aid, versionUID) => {
  return axios.post(`${config.appApiBase}/apps/${aid}/version_prod`, {
    versionUID
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.app
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const deleteGame = (gid) => {
  return axios.delete(`${config.apiBase}/games/${gid}`)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.message
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const getGameApp = (aid) => {
  return axios.get(`${config.appApiBase}/apps/${aid}`)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.app
  })
  // Append versions to App response
  .then(appResponse => {
    return axios.get(`${config.appApiBase}/apps/${aid}/versions`)
    .then(response => {
      let data = response.data

      if (!data.success) {
        return Promise.reject(data.message)
      }

      appResponse.versions = data.versions

      return appResponse
    })
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const getGameAppStatus = (aid) => {
  return axios.get(`${config.appApiBase}/apps/${aid}/status`)
  .then(statusResponse => {
    let data = statusResponse.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.status
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

/*
const getTagsByUIDs = (uids, onSuccess, onError) => {

}
*/

export default {
  getGameByGID,
  getGamesByUIDs,
  getGames,

  createGame,
  updateGameTitle,
  udpateGameDescription,
  updateGamePrice,
  updateGamePrivacy,
  updateGameBrowserSupport,
  updateGameThumbnail,
  updateGameImages,
  updateGameVideos,
  deleteGame,

  getGameApp,
  getGameAppStatus,
  createGameApp,
  updateGameAppVersion,
  updateGameAppFile
}
