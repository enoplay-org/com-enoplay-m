import config from './config'
import axios from 'axios'

const updateAlias = ({ username, alias }) => {
  return axios.post(`${config.apiBase}/users/${username}/alias`, {
    alias
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.user
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateUsername = ({ oldUsername, newUsername }) => {
  return axios.post(`${config.apiBase}/users/${oldUsername}/username`, {
    username: newUsername
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.user
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateEmail = ({ username, email }) => {
  return axios.post(`${config.apiBase}/users/${username}/email`, {
    email
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.user
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const resetEmailConfirmationCode = () => {
  return axios.post(`${config.apiBase}/users/email_confirmation_reset`)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.message
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateDescription = ({ username, description }) => {
  return axios.post(`${config.apiBase}/users/${username}/description`, {
    description
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.user
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updatePassword = ({ username, oldPassword, newPassword }) => {
  return axios.post(`${config.apiBase}/users/${username}/password`, {
    oldPassword,
    newPassword
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.message
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateIcon = ({ username, icon }) => {
  return axios.post(`${config.staticApiBase}/users/${username}/icon`, icon)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.user
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const updateBanner = ({ username, banner }) => {
  return axios.post(`${config.staticApiBase}/users/${username}/banner`, banner)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.user
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const getProfile = (refreshToken) => {
  return axios({
    method: 'get',
    url: `${config.apiBase}/sessions`,
    headers: {
      'Authorization': `Basic ${refreshToken}`
    }
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.user
  })
  // Append gamePublishHistory to User response
  .then(userResponse => {
    return axios.get(`${config.apiBase}/users/${userResponse.username}/game_publish_history`)
    .then(response => {
      let data = response.data

      if (!data.success) {
        return Promise.reject(data.message)
      }

      userResponse.gamePublishHistory = data.gamePublishHistory

      return userResponse
    })
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const register = (email, username, fullname, password, recaptcha) => {
  return axios.post(`${config.apiBase}/users`, {
    email,
    username,
    fullname,
    password,
    recaptcha
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.user
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const login = (usernameOrEmail, password) => {
  return axios({
    method: 'post',
    url: `${config.apiBase}/sessions/token_refresh`,
    withCredentials: true,
    data: {
      usernameOrEmail,
      password
    }
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return {
      refreshToken: data.refreshToken,
      profile: data.user
    }
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const verifyAccessToken = () => {
  return axios.get(`${config.apiBase}/sessions/token_access`)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.isValid
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const createAccessToken = (refreshToken) => {
  return axios({
    method: 'post',
    url: `${config.apiBase}/sessions/token_access`,
    headers: {
      'Authorization': `Basic ${refreshToken}`
    }
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.accessToken
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const logout = (refreshToken) => {
  return axios({
    method: 'delete',
    url: `${config.apiBase}/sessions/token_refresh`,
    headers: {
      'Authorization': `Basic ${refreshToken}`
    }
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.message
  })
  .catch((err) => {
    return Promise.reject(err.response.data.message)
  })
}

const confirmEmail = (code) => {
  return axios.post(`${config.apiBase}/users/email_confirmation`, {
    confirmationCode: code
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.message
  })
  .catch(err => {
    return Promise.reject(`Error confirming email code: ${err}`)
  })
}

const resetPassword = (email) => {
  return axios.post(`${config.apiBase}/users/password_reset`, {
    email
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.message
  })
  .catch((err) => {
    return Promise.reject(err.response.data.message)
  })
}

const updatePasswordWithCode = (code, password) => {
  return axios.post(`${config.apiBase}/users/password_update`, {
    resetCode: code,
    password
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.message
  })
  .catch((err) => {
    return Promise.reject(err.response.data.message)
  })
}

const resetSessionToken = () => {
  // TODO: POST /
  return new Promise((resolve, reject) => {
    resolve(null)
  })
}

const sendFeedback = ({ name, email, message }) => {
  return axios.post(`${config.apiBase}/users/feedback`, {
    name,
    email,
    message
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.message
  })
  .catch((err) => {
    return Promise.reject(err.response.data.message)
  })
}

const getGamePlayToken = (username, gid) => {
  return axios.post(`${config.apiBase}/plays`, {
    username,
    gid
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.playToken
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const getAnonGamePlayToken = (gid) => {
  return axios.post(`${config.apiBase}/plays/anonymous`, {
    gid
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data.playToken
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const verifyGamePlayAccess = (username, gid) => {
  return axios.get(`${config.apiBase}/users/${username}/game_play_access/${gid}`)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const verifyAnonGamePlayAccess = (gid) => {
  return axios.get(`${config.apiBase}/users/game_play_access/${gid}`)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const verifyGameEditAccess = (username, gid) => {
  return axios.get(`${config.apiBase}/users/${username}/game_edit_access/${gid}`)
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const purchaseGame = (username, gid, token) => {
  return axios.post(`${config.apiBase}/users/${username}/game_purchase/${gid}`, {
    token
  })
  .then(response => {
    let data = response.data

    if (!data.success) {
      return Promise.reject(data.message)
    }

    return data
  })
  .catch(err => {
    return Promise.reject(err.response.data.message)
  })
}

const addToLibrary = (username, gid) => {
  // TODO: POST /users/:user/library ; body: {game}
  return new Promise((resolve, reject) => {
    resolve('new_session_token')
  })
}

const removeFromLibrary = (username, gid) => {
  // TODO: DELETE /users/:user/library/:gid
  return new Promise((resolve, reject) => {
    resolve('new_session_token')
  })
}

export default {
  updateProfileAlias: updateAlias,
  updateProfileUsername: updateUsername,
  resetEmailConfirmationCode,
  updateProfileEmail: updateEmail,
  updateProfileDescription: updateDescription,
  updateProfilePassword: updatePassword,
  updateProfileIcon: updateIcon,
  updateProfileBanner: updateBanner,

  getProfile,
  getGamePlayToken,
  getAnonGamePlayToken,
  verifyGamePlayAccess,
  verifyAnonGamePlayAccess,
  verifyGameEditAccess,
  purchaseGame,
  addToLibrary,
  removeFromLibrary,

  register,
  login,
  verifyAccessToken,
  createAccessToken,
  logout,
  confirmEmail,
  resetPassword,
  updatePasswordWithCode,

  resetSessionToken,

  sendProfileFeedback: sendFeedback
}
