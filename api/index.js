import games from './games'
import profile from './profile'
import users from './users'
// import search from './search'

export default {
  ...games,
  ...profile,
  ...users
}
