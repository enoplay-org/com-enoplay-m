function getUserAgent (context) {
  process.userAgent = context.isServer ? context.req.headers['user-agent'] : navigator.userAgent
}

export default getUserAgent
