// const payBtnClass = 'stripe-button-el'
const formID = 'paymentForm'
const scriptID = 'paymentFormScript'
const scriptClass = 'stripe-button'
let checkoutScript
let paymentForm
let stripeHandler

const STRIPE_KEY = process.env.STRIPE_KEY

const initCheckout = (onLoaded, onFormSubmit) => {
  if (!process.BROWSER_BUILD) {
    return
  }

  paymentForm = document.getElementById(formID)
  checkoutScript = document.getElementById(scriptID)

  // Remove current script
  if (checkoutScript && checkoutScript.parentNode === paymentForm) {
    paymentForm.removeChild(checkoutScript)
  }

  // Setup new script
  let script = document.createElement('script')
  script.id = scriptID
  script.className = scriptClass
  script.setAttribute('data-key', STRIPE_KEY)

  script.onload = () => {
    onCheckoutScriptLoaded(onFormSubmit)
    onLoaded()
  }
  script.src = 'https://checkout.stripe.com/checkout.js'

  checkoutScript = script
  paymentForm.appendChild(checkoutScript)
}

const onCheckoutScriptLoaded = (onFormSubmit) => {
  stripeHandler = window.StripeCheckout.configure({
    key: STRIPE_KEY,
    token (response) {
      // Send token to onFormSubmit handler
      onFormSubmit(response.id)
    }
  })
}

const openPaymentDialog = ({ amount, name, description, email, image, locale }) => {
  stripeHandler.open({
    amount,
    name,
    description,
    email,
    image: image || `${process.env.HOST_URL_WWW}/icon.png`,
    locale: locale || '',
    closed () { }
  })
}

export default {
  initCheckout,
  openPaymentDialog,
  handler: stripeHandler
}
