import MobileDetect from 'mobile-detect'

const BROWSER_CHROME = 'chrome'
const BROWSER_FIREFOX = 'firefox'
const BROWSER_SAFARI = 'safari'
const BROWSER_EDGE = 'edge'
const BROWSER_IE = 'ie'

const DEVICE_MOBILE = 'mobile'
const DEVICE_DESKTOP = 'desktop'

const getDeviceSettings = () => {
  let ua = process.BROWSER_BUILD ? navigator.userAgent : process.userAgent
  // let browser = /Edge\/\d+/.test(ua) ? 'ed' : /MSIE 9/.test(ua) ? 'ie9' : /MSIE 10/.test(ua) ? 'ie10' : /MSIE 11/.test(ua) ? 'ie11' : /MSIE\s\d/.test(ua) ? 'ie?' : /rv:11/.test(ua) ? 'ie11' : /Firefox\W\d/.test(ua) ? 'ff' : /Chrom(e|ium)\W\d|CriOS\W\d/.test(ua) ? 'gc' : /\bSafari\W\d/.test(ua) ? 'sa' : /\bOpera\W\d/.test(ua) ? 'op' : /\bOPR\W\d/i.test(ua) ? 'op' : typeof MSPointerEvent !== 'undefined' ? 'ie?' : ''
  // let os = /Windows NT 10/.test(ua) ? 'win10' : /Windows NT 6\.0/.test(ua) ? 'winvista' : /Windows NT 6\.1/.test(ua) ? 'win7' : /Windows NT 6\.\d/.test(ua) ? 'win8' : /Windows NT 5\.1/.test(ua) ? 'winxp' : /Windows NT [1-5]\./.test(ua) ? 'winnt' : /Mac/.test(ua) ? 'mac' : /Linux/.test(ua) ? 'linux' : /X11/.test(ua) ? 'nix' : ''
  let mobile = /IEMobile|Windows Phone|Lumia/i.test(ua) ? 'w' : /iPhone|iP[oa]d/.test(ua) ? 'i' : /Android/.test(ua) ? 'a' : /BlackBerry|PlayBook|BB10/.test(ua) ? 'b' : /Mobile Safari/.test(ua) ? 's' : /webOS|Mobile|Tablet|Opera Mini|\bCrMo\/|Opera Mobi/i.test(ua) ? 1 : 0
  let tablet = /Tablet|iPad|Kindle|KFTT|KFOT|KFJWA|KFJWI|KFGIWI|KFTBWI|KFFOWI|KFSAWA|KFARWI|KFMEWI|KFASWI|KFAPWA|KFTHWA/i.test(ua)
  // let touch = 'ontouchstart' in document.documentElement

  let md = new MobileDetect(ua)

  const isMobile = mobile !== 0
  const isTablet = tablet || (process.BROWSER_BUILD ? isValidTablet() : md.tablet())
  const isDesktop = !isMobile && (process.BROWSER_BUILD ? isValidDesktop() : true)

  const browser = getBrowser() || BROWSER_CHROME
  let device = ''

  if (isDesktop) {
    device = DEVICE_DESKTOP
  } else if (isMobile || isTablet) {
    device = DEVICE_MOBILE
  }

  return {
    isMobile,
    isTablet,
    isDesktop,
    browser,
    device
  }
}

const isValidTablet = () => {
  if (window.screen.width >= 450) {
    return true
  }
  return false
}

const isValidDesktop = () => {
  if (window.screen.width >= 650) {
    return true
  }
  return false
}

const getBrowser = () => {
  let isChrome = false
  let isFirefox = false
  let isSafari = false
  let isIE = false
  let isEdge = false

  let ua = process.BROWSER_BUILD ? navigator.userAgent : process.userAgent

  if (process.BROWSER_BUILD) {
    isChrome = !!window.chrome && !!window.chrome.webstore
    isFirefox = typeof InstallTrigger !== 'undefined'
    isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === '[object SafariRemoteNotification]' })(!window['safari'] || window.safari.pushNotification)
    isIE = /* @cc_on!@ */false || !!document.documentMode
    isEdge = !isIE && !!window.StyleMedia
  } else {
    isChrome = /Chrome/.test(ua)
    isFirefox = /Firefox/.test(ua)
    isSafari = /Safari/.test(ua)
    isIE = ua.indexOf('MSIE ') > 0 || ua.indexOf('Trident/') > 0
    isEdge = ua.indexOf('Edge/') > 0
  }

  if (isChrome) {
    return BROWSER_CHROME
  } else if (isFirefox) {
    return BROWSER_FIREFOX
  } else if (isSafari) {
    return BROWSER_SAFARI
  } else if (isIE) {
    return BROWSER_IE
  } else if (isEdge) {
    return BROWSER_EDGE
  }
}

const getBrowserLogo = (browser, device) => {
  let url = ''
  let path = '/icon/browser'

  switch (browser) {
    case BROWSER_CHROME:
      path += '/chrome_64x64.png'
      break
    case BROWSER_FIREFOX:
      path += '/firefox_64x64.png'
      break
    case BROWSER_SAFARI:
      if (device === DEVICE_DESKTOP) {
        path += '/safari_64x64.png'
      } else if (device === DEVICE_MOBILE) {
        path += '/safari-ios_64x64.png'
      }
      break
    case BROWSER_IE:
      if (device === DEVICE_DESKTOP) {
        path += '/ie_64x64.png'
      }
      break
    case BROWSER_EDGE:
      if (device === DEVICE_DESKTOP) {
        path += '/edge_64x64.png'
      }
      break
    default:
      path = ''
  }

  if (path !== '') {
    url = `${process.env.HOST_URL_WWW}${path}`
  }

  return url
}

const getBrowserName = (browser) => {
  let name = ''

  switch (browser) {
    case BROWSER_CHROME:
      name = 'Google Chrome'
      break
    case BROWSER_FIREFOX:
      name = 'Mozilla Firefox'
      break
    case BROWSER_SAFARI:
      name = 'Apple Safari'
      break
    case BROWSER_IE:
      name = 'Internet Explorer'
      break
    case BROWSER_EDGE:
      name = 'Microsoft Edge'
      break
    default:
      // do nothing
  }

  return name
}

export {
  getDeviceSettings,
  getBrowserLogo,
  getBrowserName,
  BROWSER_EDGE,
  BROWSER_IE,
  DEVICE_MOBILE
}
