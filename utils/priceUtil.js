
const validPrices = [0, 99, 199, 299, 399, 499, 999, 1499]

const totalPrice = (price, currency) => {
  price = (price / 100).toFixed(2)

  switch (currency) {
    case 'usd':
      price = `$${price}`
      break
    default:
      price = `$${price}`
  }
  return price
}

export default {
  totalPrice,
  validPrices
}
