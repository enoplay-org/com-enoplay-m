const STATUS_LIVE = ''
const STATUS_PENDING = ''
const STATUS_ERROR = ''

const privacyImageBase = '/icon/privacy'

const iconFromStatus = (status) => {
  let icon = `${privacyImageBase}/pending-icon-orange-24x24.png`

  if (status === STATUS_LIVE) {
    icon = `${privacyImageBase}/check-icon-green-24x24.png`
  } else if (status === STATUS_PENDING) {
    icon = `${privacyImageBase}/pending-icon-orange-24x24.png`
  } else if (status === STATUS_ERROR) {
    icon = `${privacyImageBase}/error-icon-red-24x24.png`
  }

  return icon
}

const iconFromPrivacy = (privacy) => {
  privacy = privacy.toLowerCase()
  let icon = `${privacyImageBase}/private-icon-gray-24x24.png`

  if (privacy === 'public') {
    icon = `${privacyImageBase}/public-icon-gray-24x24.png`
  } else if (privacy === 'unlisted') {
    icon = `${privacyImageBase}/unlisted-icon-gray-24x24.png`
  } else if (privacy === 'private') {
    icon = `${privacyImageBase}/private-icon-gray-24x24.png`
  }

  return icon
}

const updateUserIcon = (source) => {
  if (!source || source.indexOf('cloudinary') < 0) {
    return source
  }

  let indexOfUpload = source.indexOf('upload/')
  let pre = source.substring(0, indexOfUpload)
  let post = source.substring(indexOfUpload).substring(7)

  return `${pre}upload/c_scale,q_auto:eco,w_70/${post}`
}

const updateUserBanner = (source) => {
  if (!source || source.indexOf('cloudinary') < 0) {
    return source
  }

  let indexOfUpload = source.indexOf('upload/')
  let pre = source.substring(0, indexOfUpload)
  let post = source.substring(indexOfUpload).substring(7)

  return `${pre}upload/c_fill,h_170,q_auto:eco,w_1024/${post}`
}

const updateGameThumbnail = (source) => {
  if (!source || source.indexOf('cloudinary') < 0) {
    return source
  }

  let indexOfUpload = source.indexOf('upload/')
  let pre = source.substring(0, indexOfUpload)
  let post = source.substring(indexOfUpload).substring(7)

  return `${pre}upload/c_fill,q_auto:eco,w_300,h_168/${post}`
}

const updateGameImage = (source) => {
  if (!source || source.indexOf('cloudinary') < 0) {
    return source
  }

  let indexOfUpload = source.indexOf('upload/')
  let pre = source.substring(0, indexOfUpload)
  let post = source.substring(indexOfUpload).substring(7)

  return `${pre}upload/c_fill,q_auto:eco/${post}`
}

// toDataURI retrieves an image and converts it to its base64 encoded version
const toDataURI = (url) => {
  return new Promise((resolve, reject) => {
    let image = new window.Image()
    image.setAttribute('crossOrigin', 'anonymous')

    image.onload = () => {
      let canvas = document.createElement('canvas')
      canvas.width = image.naturalWidth
      canvas.height = image.naturalHeight

      canvas.getContext('2d').drawImage(image, 0, 0)

      resolve(canvas.toDataURL())
    }

    image.src = url
  })
}

const dataURItoBlob = (dataURI) => {
  // convert base64/URLEncoded data component to raw binary data held in a string
  let byteString
  if (dataURI.split(',')[0].indexOf('base64') >= 0) {
    byteString = window.atob(dataURI.split(',')[1])
  } else {
    byteString = unescape(dataURI.split(',')[1])
  }

  // separate out the mime component
  let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

  // write the bytes of the string to a typed array
  let ia = new window.Uint8Array(byteString.length)
  for (let i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i)
  }

  return new window.Blob([ia], { type: mimeString })
}

export {
  iconFromStatus,
  iconFromPrivacy,
  updateUserIcon,
  updateUserBanner,
  updateGameThumbnail,
  updateGameImage,
  toDataURI,
  dataURItoBlob
}
