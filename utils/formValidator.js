import priceUtil from '../utils/priceUtil'

const passwordMinLength = 5

const passwordHasMinLength = (value) => {
  return value.length >= passwordMinLength
}

const passwordContainsLetter = (value) => {
  return value.toLowerCase().match(/[a-z]/i) || value.indexOf(' ') >= 0
}

const passwordContainsNumber = (value) => {
  return /\d/.test(value)
}

const passwordContainsUppercase = (value) => {
  return true
  // return /[A-Z]/.test(value)
}

const passwordContainsSpecial = (value) => {
  return true
  // return /[@~`!#$%^&*+=\-[\]\\';,/{}|\\":<>?]/g.test(value)
}

const isValidYoutubeVideo = (src) => {
  if (!src) {
    return false
  }
  if (src.indexOf('youtube-nocookie.com/embed') >= 0 ||
      src.indexOf('youtube.com/embed') >= 0 ||
      src.indexOf('youtu.be') >= 0 ||
      src.indexOf('youtube') >= 0) {
    return true
  }

  return false
}

const isValidTitle = (title) => {
  return title !== ''
}

const isValidDescription = (description) => {
  return description !== ''
}

const isValidPrice = (price) => {
  let isNumber = !window.isNaN(price.amount)
  let hasValidAmount = false

  for (let i = 0; i < priceUtil.validPrices.length; i++) {
    if (priceUtil.validPrices[i] === window.Number(price.amount)) {
      hasValidAmount = true
      break
    }
  }

  return isNumber && hasValidAmount
}

const isValidThumbnail = (thumbnail) => {
  return thumbnail.source != null && thumbnail.file != null
}

const isValidImages = (images) => {
  for (let i = 0; i < images.length; i++) {
    if (!images[i].source || !images[i].file) {
      return false
    }
  }
  return true
}

const isValidVideos = (videos) => {
  for (let i = 0; i < videos.length; i++) {
    if (!videos[i].source) {
      return false
    }
  }
  return true
}

const isValidBrowserSupport = (browserSupport) => {
  if (!browserSupport) {
    return false
  }

  return true
}

const isValidApp = (app) => {
  switch (app.language) {
    case 'spa': break
    case 'node': break
    case 'go': break
    default: return false
  }

  if (!app.zipFile) {
    return false
  }

  return true
}

export {
  passwordMinLength,
  passwordHasMinLength,
  passwordContainsLetter,
  passwordContainsNumber,
  passwordContainsUppercase,
  passwordContainsSpecial,
  isValidYoutubeVideo,

  isValidTitle,
  isValidDescription,
  isValidPrice,
  isValidThumbnail,
  isValidImages,
  isValidVideos,
  isValidBrowserSupport,
  isValidApp
}
