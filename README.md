# com-enoplay-m

> A web app for the mobile version of Enoplay

## Build Setup

``` bash
# install dependencies
npm install # Or yarn install

# serve with hot reload at localhost:3209
# service worker is disabled in dev
npm run dev

# build for production and launch server
npm run build
npm start

# run unit tests
npm run unit

# open test coverage report
open test/unit/coverage/lcov-report/*.html

# generate static project
npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
