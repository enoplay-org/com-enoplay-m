import Vue from 'vue'
import localforage from 'localforage'
import axios from 'axios'

import api from '~/api'
import types from '../mutation-types'
import { updateUserIcon, updateUserBanner } from '../../utils/imageUtil'

const ACCESS_TOKEN_KEY = 'ACCESS_TOKEN'
const REFRESH_TOKEN_KEY = 'REFRESH_TOKEN'
const FEEDBACK_SUBMITTED_KEY = 'feedback_submitted'

let getProfileInfoPromise

const state = {
  isLoggedIn: false,
  accessToken: '',
  refreshToken: '',

  registerAttemptStatus: null,
  loginAttemptStatus: null,

  uid: '',
  email: '',
  username: '',
  alias: '',

  description: '',
  media: { },

  library: {/* [gameUID: string]: LibraryItem */},
  gamePlayHistory: {/* [gameUID: string]: gamePlayItem */},
  gamePlayAccessHistory: {/* [gameUID: string]: GamePlayAccessItem */},
  gamePublishHistory: {/* [gameUID: string]: GamePublishItem */},

  status: '',
  settings: { }, // nightmode, etc..
  deviceSettings: { },

  isFeedbackSubmitted: false
}

const getters = {
  profile (state) {
    return {
      isLoggedIn: state.isLoggedIn,
      isActive: state.status === 'active',
      isFeedbackSubmitted: state.isFeedbackSubmitted,

      uid: state.uid,
      email: state.email,
      username: state.username,
      alias: state.alias,
      description: state.description,

      status: state.status,
      media: state.media,
      library: state.library,

      gamePublishHistory: state.gamePublishHistory,

      settings: state.settings,
      deviceSettings: state.deviceSettings
    }
  },
  deviceIsMobile (state) {
    return state.deviceSettings.isMobile || false
  },
  deviceIsTablet (state) {
    return state.deviceSettings.isTablet || false
  },
  deviceIsDesktop (state) {
    return state.deviceSettings.isDesktop || false
  },
  deviceSettingsInitialized (state) {
    return state.deviceSettings.initialized || false
  }
}
const actions = {
  updateProfileAlias ({ state, commit }, alias) {
    return api.updateProfileAlias({
      username: state.username,
      alias
    })
    .then(profile => {
      commit(types.SET_PROFILE_INFO, profile)
      return 'Updated Full Name'
    })
  },
  updateProfileUsername ({ state, commit }, username) {
    return api.updateProfileUsername({
      oldUsername: state.username,
      newUsername: username
    })
    .then(profile => {
      commit(types.SET_PROFILE_INFO, profile)
      return 'Updated Username'
    })
  },
  updateProfileEmail ({ state, commit }, email) {
    return api.updateProfileEmail({
      username: state.username,
      email
    })
    .then(profile => {
      commit(types.SET_PROFILE_INFO, profile)
      return 'Updated Email'
    })
  },
  resetEmailConfirmationCode ({ state, commit }) {
    return api.resetEmailConfirmationCode()
  },
  updateProfileDescription ({ state, commit }, description) {
    return api.updateProfileDescription({
      username: state.username,
      description
    })
    .then(profile => {
      commit(types.SET_PROFILE_INFO, profile)
      return 'Updated Description'
    })
  },
  updateProfilePassword ({ state, commit }, { oldPassword, newPassword }) {
    return api.updateProfilePassword({
      username: state.username,
      oldPassword,
      newPassword
    })
  },
  updateProfileIcon ({ state, commit }, icon) {
    return api.updateProfileIcon({
      username: state.username,
      icon
    })
    .then(profile => {
      commit(types.SET_PROFILE_INFO, profile)
      return 'Updated Icon'
    })
  },
  updateProfileBanner ({ state, commit }, banner) {
    return api.updateProfileBanner({
      username: state.username,
      banner
    })
    .then(profile => {
      commit(types.SET_PROFILE_INFO, profile)
      return Promise.resolve('Updated Banner')
    })
  },

  confirmEmail ({ state, commit }, code) {
    return api.confirmEmail(code)
  },

  resetPassword ({ state }, email) {
    return api.resetPassword(email)
  },

  updatePasswordWithCode ({ state }, { code, password }) {
    return api.updatePasswordWithCode(code, password)
  },

  // Authentication
  checkEmailAvailability (context, email) {
    return api.checkEmailAvailability(email)
  },
  checkUsernameAvailability (context, username) {
    return api.checkUsernameAvailability(username)
  },
  getProfileInfo ({ state, commit, dispatch }) {
    dispatch('retrieveFeedbackSubmitted')

    if (getProfileInfoPromise) {
      return getProfileInfoPromise
    }

    // Get refresh token from cache
    getProfileInfoPromise = dispatch('retrieveRefreshToken')
    .then(refreshToken => {
      if (!refreshToken) {
        return Promise.reject(`Error refresh token isn't available. User is not logged in`)
      }
      return refreshToken
    })
    // Generate access token
    .then(refreshToken => {
      return dispatch('createAccessToken', state.refreshToken)
      .then(accessToken => {
        return dispatch('cacheAccessToken', accessToken)
      })
    })
    // Retrieve user profile
    .then(() => {
      return api.getProfile(state.refreshToken)
      .then(profile => {
        commit(types.LOGIN_SUCCESS)
        commit(types.SET_PROFILE_INFO, profile)
      })
    })
    .then(() => {
      getProfileInfoPromise = null
    })
    .catch(() => {
      getProfileInfoPromise = null
    })

    return getProfileInfoPromise
  },
  register ({ commit, dispatch }, { email, username, alias, password, recaptcha }) {
    return dispatch('resetProfile')
    .then(() => {
      return api.register(email, username, alias, password, recaptcha)
      .then(profile => {
        commit(types.REGISTER_SUCCESS)
        commit(types.LOGIN_SUCCESS)
        commit(types.SET_PROFILE_INFO, profile)

        // Login after registration
        return dispatch('login', { usernameOrEmail: email, password })
      })
      .catch(err => {
        commit(types.REGISTER_FAILURE, err)
        commit(types.LOGIN_FAILURE)

        return Promise.reject(`Error registering user: ${err}`)
      })
    })
  },
  login ({ state, commit, dispatch }, { usernameOrEmail, password }) {
    return dispatch('resetProfile')
    .then(() => {
      // Get refresh token
      return api.login(usernameOrEmail, password)
      .then(({ refreshToken, profile }) => {
        commit(types.LOGIN_SUCCESS)
        commit(types.SET_PROFILE_INFO, profile)
        return dispatch('cacheRefreshToken', refreshToken)
      })
      // Get access token
      .then(() => {
        return dispatch('createAccessToken', state.refreshToken)
      })
      // Reload window
      .then(() => {
        window.location.reload()
      })
      .catch(err => {
        commit(types.LOGIN_FAILURE)
        return Promise.reject(`Error logging in: ${err}`)
      })
    })
  },
  logout ({ state, commit, dispatch }) {
    return api.logout(state.refreshToken)
    .then(() => {
      return dispatch('resetProfile')
    })
    .then(() => {
      window.location.reload()
    })
  },
  createAccessToken ({ dispatch }, refreshToken) {
    let accessToken = ''
    return dispatch('retrieveAccessToken')
    .then(accessTokenResponse => {
      if (!accessTokenResponse) {
        return Promise.reject(`Error access token isn't available`)
      }
      accessToken = accessTokenResponse
    })
    // Check if access token is valid
    .then(() => {
      return api.verifyAccessToken()
    })
    .then(isValid => {
      if (!isValid) {
        return Promise.reject(`Access token is invalid`)
      }

      return Promise.resolve(accessToken)
    })
    // Create new access token
    .catch(() => {
      return api.createAccessToken(refreshToken)
    })
    .catch(err => {
      console.warn('Error creating access token: ', err)
    })
  },
  cacheAccessToken ({ commit }, token) {
    return localforage.setItem(ACCESS_TOKEN_KEY, token)
    .then(() => {
      return localforage.getItem(ACCESS_TOKEN_KEY)
    })
    .then(val => {
      commit(types.SET_ACCESS_TOKEN, val)
    })
    .catch(err => {
      console.error(`Error caching access token: ${err}`)
    })
  },
  retrieveAccessToken ({ commit }) {
    return localforage.getItem(ACCESS_TOKEN_KEY)
    .then(val => {
      commit(types.SET_ACCESS_TOKEN, val)
      return val
    })
  },
  retrieveRefreshToken ({ commit }) {
    return localforage.getItem(REFRESH_TOKEN_KEY)
    .then(val => {
      commit(types.SET_REFRESH_TOKEN, val)
      return val
    })
  },
  cacheRefreshToken ({ commit }, token) {
    return localforage.setItem(REFRESH_TOKEN_KEY, token)
    .then(() => {
      return localforage.getItem(REFRESH_TOKEN_KEY)
    })
    .then(val => {
      commit(types.SET_REFRESH_TOKEN, val)
    })
    .catch(err => {
      console.error(`Error setting refresh token: ${err}`)
    })
  },
  resetProfile ({ commit }) {
    commit(types.RESET_PROFILE)
    // Remove access token
    return localforage.removeItem(ACCESS_TOKEN_KEY)
    .then(() => {
      commit(types.REMOVE_ACCESS_TOKEN)
    })
    // Remove refresh token
    .then(() => {
      return localforage.removeItem(REFRESH_TOKEN_KEY)
      .then(() => {
        commit(types.REMOVE_REFRESH_TOKEN)
      })
    })
    .catch(err => {
      console.error('Error removing access or refresh token:', err)
    })
  },
  sendFeedback ({ state, commit, dispatch }, { name, email, message }) {
    if (state.isFeedbackSubmitted) {
      return Promise.reject('Feedback is already submitted')
    }

    return api.sendProfileFeedback({
      name,
      email,
      message
    })
    .then(() => {
      return dispatch('cacheFeedbackSubmitted', true)
    })
  },
  cacheFeedbackSubmitted ({ commit }, isSubmitted) {
    return localforage.setItem(FEEDBACK_SUBMITTED_KEY, isSubmitted)
    .then(() => {
      return localforage.getItem(FEEDBACK_SUBMITTED_KEY)
    })
    .then(val => {
      commit(types.SET_FEEDBACK_SUBMITTED, val)
    })
    .catch(err => {
      console.error('Error setting feedback submitted:', err)
    })
  },
  retrieveFeedbackSubmitted ({ commit }) {
    return localforage.getItem(FEEDBACK_SUBMITTED_KEY)
    .then(val => {
      commit(types.SET_FEEDBACK_SUBMITTED, val)
      return val
    })
  }
}
const mutations = {
  [types.ADD_TO_LIBRARY] (state, { gameUID, dateAdded }) {
    var game = { uid: gameUID, dateAdded: dateAdded || Date.now() }
    Vue.set(state.library, gameUID, game)
  },
  [types.REMOVE_FROM_LIBRARY] (state, gameUID) {
    Vue.delete(state.library, gameUID)
  },
  [types.SET_PROFILE_INFO] (state, profile) {
    state.uid = profile.uid
    state.email = profile.email || state.email
    state.username = profile.username || state.username
    state.alias = profile.alias || state.alias
    state.description = profile.description || state.description
    state.status = profile.status || state.status

    if (profile.media && profile.media.icon) {
      profile.media.icon.source = updateUserIcon(profile.media.icon.source)
    }

    if (profile.media && profile.media.banner) {
      profile.media.banner.source = updateUserBanner(profile.media.banner.source)
    }

    state.media = profile.media || state.media || {}
    state.library = profile.library || state.library || {}
    state.gamePlayHistory = profile.gamePlayHistory || state.gamePlayHistory || {}
    state.gamePlayAccessHistory = profile.gamePlayAccessHistory || state.gamePlayAccessHistory || {}
    state.gamePublishHistory = profile.gamePublishHistory || state.gamePublishHistory || {}
  },
  [types.RESET_PROFILE] (state) {
    state.isLoggedIn = false
    state.uid = ''
    state.email = ''
    state.username = ''
    state.alias = ''
    state.description = ''
    state.status = ''
    state.media = {}
    state.library = {}
    state.gamePlayHistory = {}
    state.gamePlayAccessHistory = {}
    state.gamePublishHistory = {}

    state.registerAttemptStatus = null
    state.loginAttemptStatus = null
  },
  [types.REGISTER_SUCCESS] (state) {
    state.registerAttemptStatus = null
  },
  [types.REGISTER_FAILURE] (state, status) {
    state.registerAttemptStatus = status
  },
  [types.LOGIN_SUCCESS] (state) {
    state.isLoggedIn = true
    state.loginAttemptStatus = null
  },
  [types.LOGIN_FAILURE] (state) {
    state.isLoggedIn = false
  },
  [types.SET_ACCESS_TOKEN] (state, token) {
    state.accessToken = token
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
  },
  [types.REMOVE_ACCESS_TOKEN] (state) {
    state.accessToken = ''
    axios.defaults.headers.common['Authorization'] = ''
  },
  [types.SET_REFRESH_TOKEN] (state, token) {
    state.refreshToken = token
  },
  [types.REMOVE_REFRESH_TOKEN] (state) {
    state.refreshToken = ''
  },
  [types.SET_DEVICE_SETTINGS] (state, settings) {
    // window.alert(window.JSON.stringify(settings))
    settings.initialized = true
    state.deviceSettings = settings
  },
  [types.SET_FEEDBACK_SUBMITTED] (state, isSubmitted) {
    state.isFeedbackSubmitted = isSubmitted
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
