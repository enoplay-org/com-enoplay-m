import types from '../mutation-types'

const state = {
  headerTitle: 'Enoplay',
  selectedDialog: '',
  selectedSideNav: '',
  dialogTypes: {
    menu: 'menu',
    media: 'media',
    signin: 'signin',
    signup: 'signup',
    passwordReset: 'passwordReset',
    welcome: 'welcome',
    checkout: 'checkout',
    feedback: 'feedback',
    verifyEmail: 'verifyEmail',
    areYouSure: 'areYouSure',
    uploadPending: 'uploadPending',
    uploadError: 'uploadError',
    error: 'error',
    message: 'message'
  },
  sideNavTypes: {
    profile: 'profile'
  },
  show404: false,
  overlayVisible: false,
  activeMedia: { type: '', source: '' },
  darkHeader: false,
  uploadDetails: { errorMessage: '' },
  areYouSureDetails: { message: '', callback: () => {} },
  messageDetails: { title: '', content: '' }
}

const getters = {

}

const actions = {
  updateHeaderTitle ({ commit }, title) {
    commit(types.SET_HEADER_TITLE, title)
  },
  selectDialog ({ state, commit }, { dialog, data }) {
    commit(types.SET_DIALOG, { dialog, data })
    commit(types.SET_OVERLAY_VISIBLE, true)
  },
  closeDialog ({ commit }) {
    commit(types.SET_OVERLAY_VISIBLE, false)
  },
  selectSideNav ({ state, commit }, nav) {
    commit(types.SET_SIDE_NAV, nav)
    commit(types.SET_OVERLAY_VISIBLE, false)
  },
  closeSideNav ({ commit }) {
    commit(types.HIDE_SIDE_NAV)
  },
  showOverlay ({ commit }) {
    commit(types.SET_OVERLAY_VISIBLE, true)
  },
  setDarkHeader ({ commit }, isDark) {
    commit(types.SET_DARK_HEADER, isDark)
  },
  show404Message ({ commit }) {
    commit(types.SET_SHOW_404, true)
  },
  resetShow404 ({ commit }) {
    commit(types.SET_SHOW_404, false)
  }
}

const mutations = {
  [types.SET_HEADER_TITLE] (state, title) {
    state.headerTitle = title
  },
  [types.SET_DIALOG] (state, { dialog, data }) {
    state.selectedDialog = dialog

    // Media dialog
    if ((dialog === state.dialogTypes.media) && data) {
      state.activeMedia.type = data.type
      state.activeMedia.source = data.source
    }

    // Error dialog
    if ((dialog === state.dialogTypes.error) && data) {
      switch (data.type) {
        case state.dialogTypes.uploadError:
          state.selectedDialog = state.dialogTypes.uploadError
          state.uploadDetails.errorMessage = data.message
          break
        default: console.log(`Error no such error: ${data.type}`)
      }
    }

    // Message dialog
    if ((dialog === state.dialogTypes.message) && data) {
      state.messageDetails.title = data.title || ''
      state.messageDetails.content = data.content || ''
    }
    // Are you sure dialog
    if ((dialog === state.dialogTypes.areYouSure) && data) {
      state.areYouSureDetails.message = data.message || ''
      state.areYouSureDetails.callback = data.callback || (() => {})
    }
  },
  [types.SET_SIDE_NAV] (state, nav) {
    state.selectedSideNav = nav
  },
  [types.SET_DARK_HEADER] (state, isDark) {
    state.darkHeader = isDark || false
  },
  [types.HIDE_SIDE_NAV] (state) {
    state.selectedSideNav = ''
  },
  [types.SET_OVERLAY_VISIBLE] (state, isVisible) {
    if (!isVisible) {
      state.selectedDialog = ''
    }
    state.overlayVisible = isVisible
  },
  [types.SET_ACTIVE_MEDIA] (state, { type, source }) {
    state.activeMedia.type = type
    state.activeMedia.source = source
  },
  [types.SET_SHOW_404] (state, isVisible) {
    state.show404 = isVisible
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
