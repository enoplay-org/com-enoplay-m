const defaultGame = {
  uid: 'unique_id',
  gid: 'game_id',
  title: '',
  status: 'active',
  description: '',
  visibility: '',
  price: {
    amount: 0,
    unit: 'usd'
  },
  media: {
    thumbnail: { uid: '', source: '' },
    images: { },
    videos: { }
  },
  systemSupport: {
    browser: {
      chrome: { mobile: true, desktop: true },
      firefox: { mobile: true, desktop: true },
      safari: { mobile: true, desktop: true },
      edge: { mobile: true, desktop: true },
      ie: { mobile: true, desktop: true }
    }
  },
  tags: [],
  publisher: { uid: '', username: '' },
  app: { uid: '', aid: '', webURL: '' },
  stats: {
    totalPlays: 0,
    playingNow: 0
  },
  lastModifiedDate: '',
  createdDate: ''
}

const defaultUser = {
  uid: 'unique_id',
  username: 'username',
  alias: '',

  description: '',
  media: {
    icon: { uid: '', source: '' },
    banner: { uid: '', source: '' }
  },

  library: { },
  gameHistory: { },
  gamePurchaseHistory: { },

  lastOnline: ''
}

export default {
  defaultGame,
  defaultUser
}
