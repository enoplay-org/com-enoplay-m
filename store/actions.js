import api from '~/api'
import types from './mutation-types'

// Users
const getUserGamePublishHistory = ({ state, commit, dispatch }, username) => {
  return api.getUserGamePublishHistory(username)
  .then(gamePublishHistory => {
    let gameUIDs = Object.keys(gamePublishHistory || {})
    return dispatch('getGamesByUIDs', gameUIDs)
  })
}

const ensureUsers = ({ state, dispatch }, uids) => {
  return dispatch('getUsersByUIDs', uids)
}

// Games

const createGame = ({ state }, { title, description, price, thumbnail, images, videos, app }) => {
  let game = {}
  // Create Game
  return api.createGame({
    title,
    description,
    price
  })
  // Upload Media
  .then(gameResponse => {
    let gid = gameResponse.gid
    game = gameResponse

    return api.updateGameThumbnail(gid, thumbnail)
    .then(() => {
      return api.updateGameImages(gid, images)
    })
    .then(() => {
      return api.updateGameVideos(gid, videos)
    })
    .then(() => {
      return gameResponse
    })
    .catch(err => {
      return Promise.reject(`Error uploading media: ${err}`)
    })
  })
  // Create Game App
  .then(gameResponse => {
    app.append('gid', gameResponse.gid)

    return api.createGameApp(app)
    .then(() => {
      return gameResponse
    })
    .catch(err => {
      return Promise.reject(`Error creating app: ${err}`)
    })
  })
  .catch(err => {
    if (game.gid) {
      api.deleteGame(game.gid)
      .catch(() => { /* Do nothing for now */ })
    }
    return Promise.reject(`Error creating game: ${err}`)
  })
}

const getGamePlayToken = ({ state }, gid) => {
  if (state.profile.isLoggedIn) {
    return api.getGamePlayToken(state.profile.username, gid)
  } else {
    return api.getAnonGamePlayToken(gid)
  }
}

const verifyGamePlayAccess = ({ state }, gid) => {
  if (state.profile.isLoggedIn) {
    return api.verifyGamePlayAccess(state.profile.username, gid)
  } else {
    return api.verifyAnonGamePlayAccess(gid)
  }
}

const verifyGameEditAccess = ({ state }, gid) => {
  if (state.profile.isLoggedIn) {
    return api.verifyGameEditAccess(state.profile.username, gid)
  } else {
    return Promise.reject('Error: User is not logged in')
  }
}

const getActiveGame = ({ state, dispatch, commit }, gid) => {
  return dispatch('getGameByGID', gid)
  .then(game => {
    commit(types.SET_ACTIVE_GAME, game.uid)
    return dispatch('ensureUsers', [game.publisher.uid])
    .then(() => {
      return game
    })
  })
}

const getGames = ({ state, dispatch, commit }) => {
  let browser = state.profile.deviceSettings.browser
  let device = state.profile.deviceSettings.device

  return api.getGames(browser, device)
  .then(newReleaseGames => {
    commit(types.SET_NEW_RELEASE_LIST, newReleaseGames)
    return dispatch('ensureNewReleaseGames')
    .then(games => {
      let userUIDs = games.map(game => game.publisher.uid)
      return dispatch('ensureUsers', userUIDs)
    })
  })
  .catch(() => { /* Do nothing for now */ })
}

const ensureNewReleaseGames = ({ state, dispatch }) => {
  return dispatch('getGamesByUIDs', Object.keys(state.games.newReleaseList))
}

// Profile

const addToLibrary = ({ state, commit }, gameUID) => {
  let gid = state.games.all[gameUID].gid

  return api.addToLibrary(state.username, gid)
    .then(() => {
      commit(types.ADD_TO_LIBRARY, gameUID)
    })
    .catch(() => { /* Do nothing for now */ })
}

const removeFromLibrary = ({ state, commit }, gameUID) => {
  let gid = state.games.all[gameUID].gid

  return api.removeFromLibrary(state.username, gid)
  .then(() => {
    commit(types.REMOVE_FROM_LIBRARY, gameUID)
  })
  .catch(() => { /* Do nothing for now */ })
}

const ensureLibraryGames = ({ state, dispatch }) => {
  return dispatch('getGamesByUIDs', Object.keys(state.profile.library))
}

const purchaseGame = ({ state }, { gid, token }) => {
  let username = state.profile.username
  return api.purchaseGame(username, gid, token)
}

export default {
  // Users
  getUserGamePublishHistory,
  ensureUsers,

  // Games
  createGame,
  getGamePlayToken,
  verifyGameEditAccess,
  verifyGamePlayAccess,
  getActiveGame,
  getGames,
  ensureNewReleaseGames,

  // Profile
  addToLibrary,
  removeFromLibrary,
  ensureLibraryGames,
  purchaseGame
}
